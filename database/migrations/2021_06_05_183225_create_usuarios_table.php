<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id('id');
            $table->string('usuario_code');
            $table->string('usuario_nome');
            $table->date('usuario_datanascimento');
            $table->string('usuario_rua');
            $table->string('usuario_numero');
            $table->string('usuario_bairro');
            $table->string('usuario_cidade');
            $table->string('usuario_estado');
            $table->string('details');
            $table->string('logo');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
