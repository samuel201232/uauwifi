<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use DB;

class UsuarioController extends Controller
{
    public function index(){

        $usuario = DB::table('usuarios')->get();

        return view('usuario.index',compact('usuario'));
    }

    public function create(){

        return view('usuario.create');
    }
    public function store(Request $request){

        $data = array();
        $data['usuario_code'] = $request->usuario_code;
        $data['usuario_nome'] = $request->usuario_nome;
        $data['usuario_datanascimento'] = $request->usuario_datanascimento;
        $data['usuario_rua'] = $request->usuario_rua;
        $data['usuario_numero'] = $request->usuario_numero;
        $data['usuario_bairro'] = $request->usuario_bairro;
        $data['usuario_cidade'] = $request->usuario_cidade;   
        $data['usuario_estado'] = $request->usuario_cidade;     
        $data['details'] = $request->details;

        $image = $request->file('logo');
        if($image){
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success= $image->move($upload_path,$image_full_name);
            $data['logo'] = $image_url;
            $usuario = DB::table('usuarios')->insert($data);
            return redirect()->route('usuario.index')->with('success','Usuario Created Successfully');

        }


    }
    public function edit($id){
        $usuario = DB::table('usuarios')->where('id',$id)->first();
        return view ('usuario.edit',compact('usuario'));

    }
    public function update(Request $request,$id){

       // $oldlogo = $request->old_logo;

        $data = array();
        $data['usuario_nome'] = $request->usuario_nome;
        $data['usuario_code'] = $request->usuario_code;
        $data['usuario_datanascimento'] = $request->usuario_datanascimento;
        $data['usuario_rua'] = $request->usuario_rua;
        $data['usuario_numero'] = $request->usuario_numero;
        $data['usuario_bairro'] = $request->usuario_bairro;
        $data['usuario_cidade'] = $request->usuario_cidade;
        $data['usuario_estado'] = $request->usuario_estado; 

        

        $data['details'] = $request->details;

        $image = $request->file('logo');
        
        if($image){
           // unlink($oldlogo);
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success= $image->move($upload_path,$image_full_name);
            $data['logo'] = $image_url;
            $usuario = DB::table('usuarios')->where('id',$id)->update($data);
            return redirect()->route('usuario.index')->with('success','Usuario Updated Successfully');

        }
    }
    public function delete($id){
        $data = DB::table('usuarios')->where('id',$id)->first();
        $image = $data->logo;
        //unlink($image);
        $usuario = DB::table('usuarios')->where('id',$id)->delete();
        return redirect()->route('usuario.index')->with('success','Usuario Deletado com sucesso');
    }

}
