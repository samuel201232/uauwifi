@extends('usuario.layout')
@section('content')
<br><br><br>
<div class = 'containner'>
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2>Novo Usuário</h2>
</div>
<div class="pull-right">
<a class="btn btn-success" href="{{route('usuario.index')}}">Back</a>
</div>
</div>

<form action="{{route('usuario.store')}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="row">

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong> Codigo:</strong>
            <input type="text" name="usuario_code" class="form-control" placeholder="Code">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong> Nome:</strong>
            <input type="text" name="usuario_nome" class="form-control" placeholder="Nome">
    </div>
</div>


<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Data de Nascimento:</strong>
            <input type="text" name="usuario_datanascimento" class="form-control" placeholder="Data">
    </div>
</div>


<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Rua:</strong>
            <input type="text" name="usuario_rua" class="form-control" placeholder="Rua">
    </div>
</div>


<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Numero:</strong>
            <input type="text" name="usuario_numero" class="form-control" placeholder="Numero">
    </div>
</div>


<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Bairro:</strong>
            <input type="text" name="usuario_bairro" class="form-control" placeholder="Bairro">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Cidade:</strong>
            <input type="text" name="usuario_cidade" class="form-control" placeholder="Cidade">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Estado:</strong>
            <input type="text" name="usuario_estado" class="form-control" placeholder="Estado">
    </div>
</div>



<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <strong>Detalhes:</strong>
            <textarea class="form-control" name="details" style="height:150px" placeholder="Detalhes"></textarea>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <strong>Usuario Image:</strong>
            <input type="file" name="logo">
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>


</div>



</div>


</form>


@endsection