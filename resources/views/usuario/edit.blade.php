@extends('usuario.layout')
@section('content')
<br><br><br>
<div class = 'containner'>
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2>Alterar usuarios</h2>
</div>
<div class="pull-right">
<a class="btn btn-success" href="{{route('usuario.index')}}">Voltar</a>
</div>
</div>

<form action="{{url('update/usuario/'.$usuario->id)}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="row">

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Usuario Code:</strong>
            <input type="text" name="usuario_code" class="form-control" value="{{$usuario->usuario_code}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Name:</strong>
            <input type="text" name="usuario_nome" class="form-control" value="{{$usuario->usuario_nome}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Data Nascimento:</strong>
            <input type="text" name="usuario_datanascimento" class="form-control" value="{{$usuario->usuario_datanascimento}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Rua:</strong>
            <input type="text" name="usuario_rua" class="form-control" value="{{$usuario->usuario_rua}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Numero:</strong>
            <input type="text" name="usuario_numero" class="form-control" value="{{$usuario->usuario_numero}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Bairro:</strong>
            <input type="text" name="usuario_bairro" class="form-control" value="{{$usuario->usuario_bairro}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Cidade:</strong>
            <input type="text" name="usuario_cidade" class="form-control" value="{{$usuario->usuario_cidade}}">
    </div>
</div>

<div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
            <strong>Estado:</strong>
            <input type="text" name="usuario_estado" class="form-control" value="{{$usuario->usuario_estado}}">
    </div>
</div>



<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <strong>Detalhes:</strong>
            <textarea class="form-control" name="details" style="height:150px" placeholder="Details">{{$usuario->details}}</textarea>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <strong>Image:</strong>
            <input type="file" name="logo">
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <strong>Old Image:</strong>
            <img src="{{URL::to($usuario->logo)}}" heidth="70px;" width="80px;">
            <input type="hidden" name="old_logo" value="{{$usuario->logo}}">
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>


</div>



</div>


</form>


@endsection