@extends('usuario.layout')
@section('content')
<br><br><br>
<div class = 'containner'>
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2>Usuarios cadastrados</h2>
</div>
<div class="pull-right">
<a class="btn btn-success" href="{{route('create.usuario')}}">Create new produto</a>
</div>
</div>
</div>

@if($message = Session::get('success'))
<div class="alert alert-success">
<p>{{$message}}</p>
</div>
@endif


<table class= "table table-bordered">
<tr>
<th width="50px">Code</th>
<th width="280px">Name</th>
<th width="50px">Nascimento</th>
<th width="50px">Rua</th>
<th width="50px">N°</th>
<th width="50px">Bairro</th>
<th width="50px">Cidade</th>
<th width="50px">Estado</th>
<th width="200px">Details</th>
<th width="100px">Image</th>
<th width="100px">Action</th>
</tr>

@foreach($usuario as $us)
<tr>

<td>{{$us->usuario_code}}</td>
<td>{{$us->usuario_nome}}</td>
<td>{{$us->usuario_datanascimento}}</td>
<td>{{$us->usuario_rua}}</td>
<td>{{$us->usuario_numero}}</td>
<td>{{$us->usuario_bairro}}</td>
<td>{{$us->usuario_cidade}}</td>
<td>{{$us->usuario_estado}}</td>

<td>{{$us->details}}</td>
<td><img src="{{URL::to($us->logo)}}" heidth="70px;" width="80px;"></td>

<td>
<!--<a class="btn btn-info" href="">Show</a>-->
<a class="btn btn-primary" href="{{URL::to('edit/usuario/'.$us->id)}}">Edit</a>
<a class="btn btn-danger" href="{{URL::to('delete/usuario/'.$us->id)}}" onclick="return confirm('Deseja remover esse usuário?')">Delete</a>
</td>

</tr>
@endforeach



</tr>


</table>






@endsection