<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::get('/', function () {
    return view('home');
});*/



Route::get('/', function () {
    return view('main');
});

Route::get('usuarios','UsuarioController@index')->name('usuario.index');
Route::get('create','UsuarioController@create')->name('create.usuario');
Route::post('store','UsuarioController@store')->name('usuario.store');
Route::get('edit/usuario/{id}','UsuarioController@edit');
Route::get('delete/usuario/{id}','UsuarioController@delete');
Route::post('update/usuario/{id}','UsuarioController@update');




